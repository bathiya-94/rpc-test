package main

import (
	"log"
	"net/rpc"
	"time"

	"rpc-test/common"
)

func printList(vegetables []common.Vegetable) {
	if len(vegetables) == 0 {
		log.Println("No vegetables in the database yet")
	} else {
		for _, vegetable := range vegetables {
			log.Printf("%s \t Price per kg: Rs. %0.2f \t Available Quantity: %0.3fkg", vegetable.Name,
				vegetable.Price, vegetable.Quantity)
		}
	}
}

func main() {
	var vegetablesDb []common.Vegetable
	var vegetable common.Vegetable
	var priceChange common.VariableChange
	var quantityChange common.VariableChange

	client, err := rpc.DialHTTP("tcp", "localhost:4040")

	if err != nil {
		log.Fatal("Connection error: ", err)
	} else {
		log.Print("Client Started and Running")
	}

	time.Sleep(2 * time.Second)

	if err := client.Call("RpcServer.GetVegetables", common.Vegetable{}, &vegetablesDb); err != nil {
		log.Println("[GetVegetables] Error occurred when retrieving the available list of vegetables: ", err)
	} else {
		log.Println("[GetVegetables] successful. The available List of vegetables: ")
		printList(vegetablesDb)
	}

	if err := client.Call("RpcServer.AddVegetable",
		common.Vegetable{Name: "cucumber", Price: 15.00, Quantity: 1500.250}, &vegetablesDb); err != nil {
		log.Println("[AddVegetable] Error occurred when adding new vegetable: ", err)
	} else {
		log.Println("[AddVegetable] successful. Updated List: ")
		printList(vegetablesDb)
	}

	if err := client.Call("RpcServer.EditPrice",
		common.VariableChange{Vegetable: "beans", Value: 20.00}, &vegetable); err != nil {
		log.Println("[EditPrice] Error occurred when editing price: ", err)
	} else {
		log.Printf("[EditPrice] successful. The current price per kg of %s: Rs.%0.2f \n",
			vegetable.Name, vegetable.Price)
	}

	if err := client.Call("RpcServer.EditQuantity",
		common.VariableChange{Vegetable: "beans", Value: 2500.500}, &vegetable); err != nil {
		log.Println("[EditQuantity] Error occurred when editing quantity: ", err)
	} else {
		log.Printf("[EditQuantity] successful. Available Quantity of %s: %0.3fkg\n",
			vegetable.Name, vegetable.Quantity)
	}

	if err := client.Call("RpcServer.GetPrice", "beans", &priceChange); err != nil {
		log.Println("[GetPrice] Error occurred when retrieving price: ", err)
	} else {
		log.Printf("[GetPrice] successful. The current price kg of %s: Rs.%0.2f \n", priceChange.Vegetable,
			priceChange.Value)
	}

	if err := client.Call("RpcServer.GetQuantity", "beans", &quantityChange); err != nil {
		log.Println("[GetQuantity] Error occurred when retrieving quantity: ", err)
	} else {
		log.Printf("[GetQuantity] successful. Availabe Quantity of %s: %0.3fkg \n", quantityChange.Vegetable,
			quantityChange.Value)
	}
}
