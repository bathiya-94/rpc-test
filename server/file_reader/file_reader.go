package file_reader

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"

	"rpc-test/common"
)

func RetrieveData() []common.Vegetable{

	var vegetables [] common.Vegetable
	records, err := readData("vegetables.csv")

	if err != nil {
		log.Fatal("Error in reading from file",err)
	}

	for _, record := range records {

		var convertedPrice float64
		var convertQuantity float64

		if price, err := strconv.ParseFloat(record[1], 32); err != nil {
			log.Fatalln("Error in Converting Price ", err)
		} else {
			convertedPrice = price
		}

		if quantity, err := strconv.ParseFloat(record[2], 32); err != nil {
			log.Fatalln("Error in Converting Quantity ", err)
		} else {
			convertQuantity = quantity
		}

		veg := common.Vegetable{
			Name:     record[0],
			Price:    convertedPrice,
			Quantity: convertQuantity,
		}
		vegetables = append(vegetables, veg)
	}
	return vegetables
}

func readData(fileName string) ([][]string, error) {

	f, err := os.Open(fileName)

	if err != nil {
		return [][]string{}, err
	}

	defer f.Close()

	r := csv.NewReader(f)

	// skip first line
	if _, err := r.Read(); err != nil {
		return [][]string{}, err
	}

	records, err := r.ReadAll()

	if err != nil {
		return [][]string{}, err
	} else {
		log.Println("Read data from file successfully")
	}

	return records, nil
}