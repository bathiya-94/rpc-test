package filewriter

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"

	"rpc-test/common"
)

func writeToFile(output [][]string, filename string) {
	f, err := os.Create(filename)
	defer f.Close()

	if err != nil {

		log.Fatalln("failed to open file", err)
	}

	w := csv.NewWriter(f)
	defer w.Flush()

	for _, record := range output {
		if err := w.Write(record); err != nil {
			log.Fatalln("error writing record to file", err)
		}
	}
}

func SaveChanges(vegetables []common.Vegetable ) {

	var outputString [][]string

	titles := []string{"Vegetables", "Price", "Quantity"}
	outputString = append(outputString, titles)

	for _, veg := range vegetables {
		var vegR []string
		var price = fmt.Sprintf("%0.2f", veg.Price)
		var quantity = fmt.Sprintf("%0.3f", veg.Quantity)
		vegR = append(vegR, veg.Name)
		vegR = append(vegR, price)
		vegR = append(vegR, quantity)
		outputString = append(outputString, vegR)
	}

	writeToFile(outputString, "vegetables.csv")
}
