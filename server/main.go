package main

import (
	"errors"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"strings"

	"rpc-test/common"
	"rpc-test/server/file_reader"
	"rpc-test/server/file_writer"
)

type RpcServer struct{}

func (a *RpcServer) GetPrice(vegetable string, reply *common.VariableChange) (err error) {
	log.Println("[Get Price] request for ", vegetable)
	var dataToSend common.VariableChange

	var vegetableList = file_reader.RetrieveData()
	isFound := false

	for _, veg := range vegetableList {
		if strings.ToLower(veg.Name) == strings.ToLower(vegetable) {
			dataToSend = common.VariableChange{Vegetable: veg.Name, Value: veg.Price}
			isFound = true
		}
	}
	if isFound {
		*reply = dataToSend
		return nil
	} else {
		*reply = common.VariableChange{}
		err = errors.New("vegetable not found in the database")
		log.Printf("[Get Price] %s not found in database", vegetable)
		return err
	}
}

func (a *RpcServer) GetQuantity(vegetable string, reply *common.VariableChange) (err error) {
	log.Println("[Get Quantity] request for ", vegetable)
	var dataToSend common.VariableChange

	var vegetableList = file_reader.RetrieveData()
	isFound := false

	for _, veg := range vegetableList {
		if strings.ToLower(veg.Name) == strings.ToLower(vegetable) {
			dataToSend = common.VariableChange{Vegetable: veg.Name, Value: veg.Quantity}
			isFound = true
		}
	}

	if isFound {
		*reply = dataToSend
		return nil
	} else {
		*reply = common.VariableChange{}
		err = errors.New("vegetable not found in the database")
		log.Printf("[Get Quantity] %s not found in database", vegetable)
		return err
	}

}

func (a *RpcServer) GetVegetables(vegetable common.Vegetable, db *[]common.Vegetable) error {
	log.Println("[Get Vegetables] request ")
	*db = file_reader.RetrieveData()
	return nil
}

func (a *RpcServer) AddVegetable(vegetable common.Vegetable, reply *[]common.Vegetable) (err error) {
	log.Println("[Add Vegetable] request for ", vegetable)
	vegetableList := file_reader.RetrieveData()
	doesExists := false

	for _, veg := range vegetableList {
		if strings.ToLower(veg.Name) == strings.ToLower(vegetable.Name) {
			doesExists = true
		}
	}

	if !doesExists {
		vegetableList = append(vegetableList, vegetable)
		filewriter.SaveChanges(vegetableList)
		newVegList := file_reader.RetrieveData()
		log.Println("[Add Vegetable] Vegetable List Updated: ", newVegList)
		*reply = newVegList
		return nil
	} else {
		*reply = vegetableList
		err = errors.New("entry for the vegetable already exists")
		log.Printf("[Add Vegetable] Record for %s already exists\n", vegetable.Name)
		return err
	}

}

func (a *RpcServer) EditPrice(edit common.VariableChange, reply *common.Vegetable) (err error) {

	log.Printf("[Edit Price] request for %s with value %0.2f\n", edit.Vegetable, edit.Value)
	var changed common.Vegetable

	flagWriteReq := false
	var vegetableList = file_reader.RetrieveData()
	for idx, vegetable := range vegetableList {
		if strings.ToLower(vegetable.Name) == strings.ToLower(edit.Vegetable) {
			vegetableList[idx] = common.Vegetable{Name: vegetable.Name, Price: edit.Value,
				Quantity: vegetable.Quantity}
			changed = vegetableList[idx]
			flagWriteReq = true
		}
	}

	if flagWriteReq {
		filewriter.SaveChanges(vegetableList)
		log.Println("Vegetable List Updated: ", file_reader.RetrieveData())
		*reply = changed
		return nil
	} else {
		*reply = common.Vegetable{}
		err = errors.New("vegetable not found in the database")
		log.Printf("[Edit Price] %s not found in database", edit.Vegetable)
		return err
	}

}

func (a *RpcServer) EditQuantity(edit common.VariableChange, reply *common.Vegetable) (err error) {

	log.Printf("[Edit Quantity] request for %s with value %0.2f\n", edit.Vegetable, edit.Value)

	var changed common.Vegetable

	flagWriteReq := false
	var vegetableList = file_reader.RetrieveData()
	for idx, vegetable := range vegetableList {
		if strings.ToLower(vegetable.Name) == strings.ToLower(edit.Vegetable) {
			vegetableList[idx] = common.Vegetable{Name: vegetable.Name, Price: vegetable.Price,
				Quantity: edit.Value}
			changed = vegetableList[idx]
			flagWriteReq = true
		}
	}

	if flagWriteReq {
		filewriter.SaveChanges(vegetableList)
		log.Println("Vegetable List updated: ", file_reader.RetrieveData())
		*reply = changed
		return nil
	} else {
		*reply = common.Vegetable{}
		err = errors.New("vegetable not found in the database")
		log.Printf("[Edit Quantity] %s not found in database\n", edit.Vegetable)
		return err
	}

}

func main() {

	file_reader.RetrieveData()

	var api = new(RpcServer)
	err := rpc.Register(api)

	if err != nil {
		log.Fatalln("Error registering the RpcServer", err)
	}

	rpc.HandleHTTP()

	listener, err := net.Listen("tcp", ":4040")

	if err != nil {
		log.Fatal("Listener error", err)
	}

	log.Printf("Serving RPC on port %d", 4040)
	err = http.Serve(listener, nil)

	if err != nil {
		log.Fatal("Error Serving: ", err)
	}
}
