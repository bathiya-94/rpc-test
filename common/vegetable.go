package common

type Vegetable struct {
	Name string
	Price  float64
	Quantity float64
}

type VariableChange struct {
	Vegetable string
	Value     float64
}